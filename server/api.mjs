// require('dotenv').config();
import express from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'
import connection from './db/sql.connection.mjs'

import user_router from './modules/user/user.router.mjs';

const { PORT,HOST } = process.env;

const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

app.set("trust proxy", 1) // trust first proxy
// routing
// app.use('/api/stories', story_router);
app.use('/api/users', user_router);


//when no routes were matched...
app.use('*', (req,res)=>{
    res.send("NOT FOUND")
})

;(async ()=> {
  //connect to mongo db
  await app.listen(PORT,HOST);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)